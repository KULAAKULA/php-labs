<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Lab2</title>
</head>

<body class="d-flex flex-column h-100">
    <?php include('header.php') ?>
    <main class="flex-shrink-0">
        <div class="container">
            <form action="https://httpbin.org/post" method="post" class="container d-flex flex-column justify-content-center ">
                <div class="form-group mb-2">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" id="username">
                </div>
                <div class="form-group mb-2">
                    <label for="email-address">Email</label>
                    <input type="email" name="email-address" class="form-control" id="email-address">
                </div>
                <select name="appeal" class="form-control mb-2">
                    <option value="complaint">Жалоба</option>
                    <option value="proposal">Предложение</option>
                    <option value="gratitude">Благодарность</option>
                </select>
                <textarea name="appeal_text" class="form-control mb-2"></textarea>
                <div class="form-check mb-2">
                    <input type="checkbox" name="sms" id="sms" class="form-check-input"><label for="sms" class="form-check-label">SMS</label></input>
                </div>
                <div class="form-check mb-2">
                    <input type="checkbox" name="email" id="email" class="form-check-input"><label for="email" class="form-check-label">Email</label></input>
                </div>
                <button type="submit" class="btn btn-primary">Отправить</button><br />
                <a href="page2.php">Вторая страница</a>
            </form>
        </div>
    </main>
    <?php include('footer.php') ?>
</body>

</html>