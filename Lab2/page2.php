<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Lab2</title>
</head>

<body class="d-flex flex-column h-100">
    <?php include('header.php');
    echo "<div class='container'>";
    echo "<textarea rows='15' class='form-control' readonly>    ";
    print_r(get_headers("https://httpbin.org/get"));
    echo "</textarea>";
    echo "<a href='index.php'>Главная страница</a>";
    echo "</div>";
    include('footer.php');
    ?>
</body>

</html>