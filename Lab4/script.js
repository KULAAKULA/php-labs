const output = document.querySelector(".output");
const result = document.querySelector(".result");
const keys = document.querySelectorAll(".btn");

keys.forEach((key) => {
    key.addEventListener("click", calculate);
});

function calculate() {
    let buttonText = this.innerText;

    if (buttonText === "AC") {
        output.innerText = "";
        result.innerText = "0";
        return;
    } else if (buttonText === "DEL") {
        output.textContent = output.textContent.substring(
            0,
            output.textContent.length - 1,
        );
        return;
    } else if (buttonText === "=") {
        try {
            result.innerText = eval(output.innerText).toString().substring(0, 9);
        } catch (error) {
            result.innerText = 'Ошибка'
        }
    } else {
        output.textContent += buttonText;
    }
}
